using GLib;
using Gee.XmlEtree;

public class BasicSample : Object {
	
	public void run( ) {
		var etree = new ElementTree( );
		
		try {
			etree.parse_file( "test2.xml" );
		}catch( Gee.XmlEtree.Error e ){
			stderr.printf( "Oops: %s\n", e.message );
		}
		
		// Finding Elements with Xpath
		// ----------------------------
		
		Gee.List<Element> list = etree.find( "/addressbook/email" ) ;
		int i = 1 ;
		foreach( Element e in list ) {
			stdout.printf( "Email_%i : %s\n", i, e.text );
			i++;
		}
	}
	
	public static int main( string[] args ) {
		var sample = new BasicSample( );
		
		sample.run( );
		return 0;
	}
}
