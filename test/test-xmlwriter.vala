using Gee.XmlEtree;

public class BasicSample : Object {
	
	public void run( ) {
		ElementTree etree ;
		Element sub_e ;
		
		
		etree = new ElementTree( );
		etree.root = new Element( "note" ) ;
		
		// Appending SubElements
		sub_e = new SubElement( etree.root, "to" );
		sub_e.text = "Harry" ;
		sub_e.set_attrib( "city", "London" );
		
		sub_e = new SubElement( etree.root, "from" );
		sub_e.text = "John" ;
		sub_e.set_attrib( "city", "New York" );
		
		sub_e = new SubElement( etree.root, "heading" );
		sub_e.text = "Reminder" ;
		
		// Another way to append SubElements
		sub_e = new Element( "body" );
		sub_e.text = "Don't forget me this weekend!" ;
		etree.append( sub_e );
		
		etree.write( "note.xml" );
	}
	
	public static int main( string[] args ) {
		var sample = new BasicSample( );
		
		sample.run( );
		return 0;
	}
}
