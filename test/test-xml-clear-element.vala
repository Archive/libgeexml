using Gee.XmlEtree;

public class BasicSample : Object {
	
	private ElementTree etree ;

	private void create_xml () {
		Element sub_e ;
		
		etree = new ElementTree( );
		etree.root = new Element( "note" ) ;
		
		// Appending SubElements
		sub_e = new SubElement( etree.root, "to" );
		sub_e.text = "Harry" ;
		sub_e.set_attrib( "city", "London" );
		
		sub_e = new SubElement( etree.root, "from" );
		sub_e.text = "John" ;
		sub_e.set_attrib( "city", "New York" );
	}

	private void clear_element ( string tag ) {
		foreach( Element e in etree ) {
			if (e.tag == tag ) 
				e.clear() ;
		}
	}

	public void run( ) {
		this.create_xml() ;
		this.clear_element("to");
		this.etree.write("note-cleared-element.xml");
	}
	
	public static int main( string[] args ) {
		var sample = new BasicSample( );
		
		sample.run( );
		return 0;
	}
}
