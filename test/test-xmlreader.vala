using GLib;
using Gee.XmlEtree;

public class BasicSample : Object {
	
	public void run( ) {
		var etree = new ElementTree( );
		
		try {
			etree.parse_file( "test1.xml" );
		}catch( Gee.XmlEtree.Error e ){
			stderr.printf( "Oops: %s\n", e.message );
		}

		this.print_elements( etree , 0 ); 
	}
	
	private void print_elements( Element etree , int level ){
		foreach( Element e in etree ) {
			stdout.printf ("%s %s : '%s' attrs=[%s] \n", this.make_arrows(level), e.tag, 
								e.text, this.print_attrs(e)) ;
			this.print_elements( e, level + 1); 
		}	
	} 

	private string print_attrs( Element e ) {
		string s = "" ;

		foreach( string key in e.attribs.get_keys( ) ) {
			s = s + "'%s' : '%s', ".printf (key, e.attribs[key]) ;
		}
		
		return s ;
	}

	private string make_arrows (int level){
		string s = "";
		int i ;

		for ( i = 0 ; i<level ; i++ ){
			s = s + "--" ;
		}
		s = s + ">" ;
		return s ;
	}


	public static int main( string[] args ) {
		var sample = new BasicSample( );
		
		sample.run( );
		return 0;
	}
}
