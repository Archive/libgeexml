using GLib;
using Gee.XmlEtree;

public class BasicSample : Object {
	
	public void run( ) {
		var etree = new ElementTree( );
		var xml_data = new StringBuilder ();

		xml_data.append ("<?xml version=\"1.0\"?>");
		xml_data.append ("<note><to>Harry</to><from>John</from>");
		xml_data.append ("<heading>Reminder</heading><body>Don't forget me this weekend!</body></note>");

		try {
			etree.parse_string( xml_data.str );
		}catch( Gee.XmlEtree.Error e ){
			stderr.printf( "Oops: %s\n", e.message );
		}
		
		
		// Iterating xml elements
		// --------------------------------
		
		foreach( Element e in etree ) {
			stdout.printf( "<%s>\n" , e.tag );
			foreach( string key in e.attribs.get_keys( ) ) {
				stdout.printf( " |-> '%s' : '%s'\n", key, e.attribs[key] ) ;
			}
			if ( e.tag != "note" ) 
			stdout.printf( " \\-> text : '%s'\n", e.text );
		}
	
	}
	
	public static int main( string[] args ) {
		var sample = new BasicSample( );
		
		sample.run( );
		return 0;
	}
}
