/* ElementTree.vala
 *
 * Copyright (C) 2009 OpenShine S.L
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 * 	Roberto Majadas <roberto.majadas@openshine.com>
 */

using GLib;
using Xml;

namespace Gee.XmlEtree {
	
	public errordomain Error {
		FILE_NOT_FOUND,
		XML_DOCUMENT_EMPTY
	}
	
	public class ElementTree : Element {
		
		private Xml.Doc* _doc ;
		
		public ElementTree( ) {
		
		}
		
		~ElementTree( ) {
			if ( this._doc != null ) {
				delete this._doc ;
			}
		}
		
		public Element root {
			get {
				return this ;
			}
			
			set {
				this.node = value.node ;
			}
		}
		
		public void parse_file( string path ) throws Error {
			this._doc = Parser.parse_file( path );
			if ( this._doc == null ) {
				throw new Error.FILE_NOT_FOUND( "file %s not found or permissions missing", path );
			}
			
			this.node = this._doc->get_root_element( );
			if ( this.node == null ) {
				delete this._doc;
				throw new Error.XML_DOCUMENT_EMPTY( "the xml'%s' is empty", path );
			}
		}

		public void parse_string( string xml_data ) throws Error {
			this._doc = Parser.parse_memory( xml_data, (int) xml_data.size() );
			if ( this._doc == null ) {
			    throw new Error.FILE_NOT_FOUND( "It's not posible to parse this string" );
			}
			
			this.node = this._doc->get_root_element( );
			if ( this.node == null ) {
				delete this._doc;
				throw new Error.XML_DOCUMENT_EMPTY( "the xml string is empty" );
			}
		}
		
		public void write( string path ) {
			if ( this._doc == null ) {
				this._doc = new Xml.Doc( "1.0" );
				if ( this.node != null ) 
				this._doc->set_root_element( this.node ) ;
				
				this._doc->save_format_file( path, this._doc, 1 );
			}
		}
		
		public Gee.List find( string path ) {
			Xml.XPathContext ctx ;
			Xml.XPathObject* objs ;
			Gee.List<Element> list = new ArrayList<Element>( );
			
			int i ;
			
			ctx = new XPathContext( this._doc ) ;
			objs = ctx.eval_expression( path );
			
			if ( objs->nodesetval == null ) 
			return list;
			
			for ( i = 0 ; i < objs->nodesetval->length( ) ; i++ ) {
				Element fnode ;
				Xml.Node* node ;
				
				fnode = new Element( null );
				node = objs->nodesetval->item( i ) ;
				fnode.node = node ;
				list.add( fnode );
			}
			
			return list;
		}
	
	}
	
	public class SubElement : Element {
		
		public SubElement( Element e , string tag ) {
			this.node = new Xml.Node( null, tag );
			e.node->add_child( this.node );			
		}
	
	}
	
	public class Element : Gee.Iterable<Element>, 
	Gee.CollectionObject<Element> 
	{
		private Xml.Node* _node ;
		private Xml.Node* _next_node ;
		private Gee.HashMap<string, string> _attrs ;
		
		public Element( string? tag ) {
			if ( tag != null ){
				this.node = new Xml.Node( null, tag );
			}								
		}
		
		public Xml.Node* node {
			get { return _node; }
			set { 
				Xml.Node* iter ;
				if ( value->type != ElementType.ELEMENT_NODE ){
					iter = value->next ;
					while ( iter != null ) {
						if ( iter->type == ElementType.ELEMENT_NODE ){
							this._node = iter ;	
							break;
						}
						iter = iter->next ; 
					}
					
					if ( this._node == null ){
						this._next_node = null ;
						return;
					}
				}else{
					this._node = value;
				}
				
				iter = this._node->next ;
				while ( iter != null ) {
					if ( iter->type == ElementType.ELEMENT_NODE ){
						this._next_node = iter ;
						return ;
					}
					iter = iter->next ; 
				}
				this._next_node = null ;
			}
		}
		
		public Xml.Node* next_node {
			get { return this._next_node ; }
		}
		
		public string tag {
			get { 
				if ( this.node != null ){
					return this.node->name ;
				}else{
					return "" ;
				}
			}
			
			set {
				if ( this.node != null ){
					this.node->set_name( value ) ;
				}else{
					this.node = new Xml.Node( null, value );
				}
			}
		}
		
		public Gee.HashMap<string, string> attribs {
			get {
				if ( this._attrs != null ) 
				return this._attrs ;
				
				this.lookup_node_attrs( ) ;
				return this._attrs ;
			}
		}
		
		public string text {
			get {
				Xml.Node* iter ;
				iter = this.node->children ;
				while ( iter != null ) {
					if ( iter->type == ElementType.TEXT_NODE ) {
						return iter->content ;
					}
					iter = iter->next ;
				}
				return "";
			}
			set {
				if ( this.node != null )
				this.node->set_content( value ) ;
			}
		}
		
		public void append( Element e ) {
			this.node->add_child( e.node );
		}
		
		public void clear () {
			Xml.Node* new_node;
			
			new_node = new Xml.Node( null, this.tag );
			this.node = this.node->replace( new_node ) ;
			
		}
		
		public void set_attrib( string attr, string value ) {
			if ( this.node != null ){
				this.node->set_prop( attr, value );
				this.lookup_node_attrs( ) ;
			}
		}
		
		// Private Methods
		// -------------------------------------------------
		
		private void lookup_node_attrs( ) {
			this._attrs = new Gee.HashMap<string, string>( GLib.str_hash, GLib.str_equal );
			
			Xml.Attr* attr_iter = this.node->properties ;
			while ( attr_iter != null ) {
				this._attrs.set( attr_iter->name, attr_iter->children->content ) ;
				attr_iter = attr_iter->next ;
			}
		}
		
		// Interation code
		// --------------------------------------------------
		
		
		public Type get_element_type( ) {
			return typeof( Element );
		}
		
		public Gee.Iterator<Element> iterator( ) {
			return new Iterator<Element>( this );
		}
		
		private class Iterator<T> : Gee.Iterator<T>,
		Gee.CollectionObject<T> {
			
			private Element _iter ;
			private Element iter { 
				get { return this._iter ; } 
				set {
					if ( this._iter == null ) {
						this._iter = new Element (null) ;
						this._iter.node = value.node->children ;
					} 
					else {
						this._iter = value ;
					}
				}
			}
			
			public Iterator( Element e ) {
				this.iter = e ;
			}
			
			public bool next( ) {
				if ( this.iter.node == null ) {
					return false ;
				} else {
					return true ;
				}
			}
			
			public T? get( ) {
				var ret = this.iter ;
				var next = new Element ( null ) ;

				if (this.iter.next_node != null) {
					next.node = this.iter.next_node ;
				}

				this.iter = next ;
				return ret ;
			}
		
		}
	
	}
}
